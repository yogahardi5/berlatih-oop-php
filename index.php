<!DOCTYPE html>
<html>
<head>
	<title>Belajar OOP</title>
</head>
<body>
	<?php
		include ("animal.php");
		include ("Frog & Ape.php");
		$sheep = new Animal();
		
		$sheep->set_name("Shaun");
		$sheep->set_legs(2);
		$sheep->set_cold_blooded("False");

		echo "Nama Hewan ini adalah : ".$sheep->get_name()."<br>Jumlah Kaki ".$sheep->get_name()." Sebanyak : ".$sheep->get_legs()."<br>Apakah ".$sheep->get_name()." berdarah dingin ? : ".$sheep->get_cold_blooded();

		$sungokong = new Ape();
		$sungokong->set_name("Kera Sakti");
		$sungokong->set_yell("Auooo");
		$sungokong->set_legs(4);
		$sungokong->set_cold_blooded("False");

		echo "<br><br>Nama Hewan ini adalah : ".$sungokong->get_name()."<br>Jumlah Kaki ".$sungokong->get_name()." Sebanyak : ".$sungokong->get_legs()."<br>Apakah ".$sungokong->get_name()." berdarah dingin ? : ".$sungokong->get_cold_blooded()."<br>".$sungokong->get_name()." bersuara : ".$sungokong->get_yell();

		$kodok = new Frog();
		$kodok->set_name("Buduk");
		$kodok->set_jump("Hop Hop");
		$kodok->set_legs(4);
		$kodok->set_cold_blooded("True");

		echo "<br><br>Nama Hewan ini adalah : ".$kodok->get_name()."<br>Jumlah Kaki ".$kodok->get_name()." Sebanyak : ".$kodok->get_legs()."<br>Apakah ".$kodok->get_name()." berdarah dingin ? : ".$kodok->get_cold_blooded()."<br>".$kodok->get_name()." berjalan dengan : ".$kodok->get_jump();
	 ?>
</body>
</html>