<?php
	class animal
	{
		protected $name;
		function set_name($new_name){
			$this->name = $new_name;
		}

		function get_name(){
			return $this->name;
		}

		protected $legs;
		function set_legs($new_legs){
			$this->legs = $new_legs;
		}

		function get_legs(){
			return $this->legs;
		}

		protected $cold_blooded;
		function set_cold_blooded($new_cold_blooded){
			$this->cold_blooded = $new_cold_blooded;
		}

		function get_cold_blooded(){
			return $this->cold_blooded;
		}
	}		




?>